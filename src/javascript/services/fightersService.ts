import { callApi } from '../helpers/apiHelper';
import { Fighter } from '../models/Fighter';

export async function getFighters() {
  try {
    const endpoint = 'fighters.json';
    const apiResult = await callApi(endpoint, 'GET');
    
    return apiResult;
  } catch (error) {
    throw error;
  }
}

export async function getFighterDetails(id: number): Promise<Fighter> {
  const endpoint = `details/fighter/${id}.json`;
  const fighter = await callApi(endpoint, 'GET') as Fighter;

  return fighter;
}

