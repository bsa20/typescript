export interface Fighter{
    _id: number;
    name: string;
    health: number;
    attack: number;
    defense: number;
    source: string;
}