import { Fighter } from "./models/Fighter";

export function fight(firstFighter: Fighter, secondFighter: Fighter): Fighter {
  let first = true;
  let firstFighterHealth = firstFighter.health;
  let secondFighterHealth = secondFighter.health;

  while(firstFighterHealth > 0 && secondFighterHealth > 0){
    if(first){
      let hit = getHitPower(firstFighter);
      let block = getBlockPower(secondFighter);
      let damage = getDamage(hit, block);
      secondFighterHealth -= damage >= 0 ? damage : 0;
    } else {
      let hit = getHitPower(secondFighter);
      let block = getBlockPower(firstFighter);
      let damage = getDamage(hit, block);
      firstFighterHealth -= damage >= 0 ? damage : 0;
    }
    first = !first;
  }

  return firstFighterHealth <= 0 ? secondFighter : firstFighter;
}

export function getDamage(hit: number, block: number): number {
  return hit - block;
}

export function getHitPower(fighter: Fighter): number {
  const criticalHitChance = getRandomArbitrary(1,2);
  return fighter.attack * criticalHitChance;
}

export function getBlockPower(fighter: Fighter): number {
  const dodgeChance = getRandomArbitrary(1,2);
  return fighter.defense * dodgeChance;
}

function getRandomArbitrary(min: number, max: number): number {
  return Math.random() * (max - min) + min;
}
