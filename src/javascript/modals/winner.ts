import { createElement } from "../helpers/domHelper";
import { Fighter } from "../models/Fighter";
import { showModal } from "./modal";

export  function showWinnerModal(fighter: Fighter) {
  const title = 'Winner: ' + fighter.name;

  const winnerDetails = createElement({ tagName: 'div', className: 'modal-body' }) as HTMLElement;

  const imageElement = createElement({ tagName: 'img', className: 'fighter-image', attributes: {src: fighter.source}}) as HTMLElement;
  winnerDetails.append(imageElement);

  showModal({ title: title, bodyElement: winnerDetails });
}