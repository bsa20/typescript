import { createElement } from '../helpers/domHelper';
import { showModal } from './modal';
import { Fighter } from '../models/Fighter';

export  function showFighterDetailsModal(fighter) {
  const title = 'Fighter info';
  const bodyElement = createFighterDetails(fighter);
  showModal({ title, bodyElement });
}

function createFighterDetails(fighter: Fighter): HTMLElement {
  const fighterDetails = createElement({ tagName: 'div', className: 'modal-body' }) as HTMLElement;
  const nameElement = createElement({ tagName: 'div', className: 'fighter-name' }) as HTMLElement;
  nameElement.innerText = "Name: "+ fighter.name;
  fighterDetails.append(nameElement);

  const attackElement = createElement({ tagName: 'div', className: 'fighter-attack'}) as HTMLElement;
  attackElement.innerText = "Attack: "+ fighter.attack;
  fighterDetails.append(attackElement);

  const defenseElement = createElement({ tagName: 'div', className: 'fighter-defense'}) as HTMLElement;
  defenseElement.innerText = "Defence: "+ fighter.defense;
  fighterDetails.append(defenseElement);

  const healthElement = createElement({ tagName: 'div', className: 'fighter-health'}) as HTMLElement;
  healthElement.innerText = "Health: "+ fighter.health;
  fighterDetails.append(healthElement);

  const imageElement = createElement({ tagName: 'img', className: 'fighter-image', attributes: {src: fighter.source}}) as HTMLElement;
  fighterDetails.append(imageElement);

  return fighterDetails;
}
